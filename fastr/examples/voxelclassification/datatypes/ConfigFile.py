# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ConfigParser

from fastr.datatypes import URLType
from fastr.helpers.checksum import hashsum


class ConfigFile(URLType):
    description = 'Config file following the ini style'
    extension = 'conf'

    def checksum(self):
        """
        Return the checksum of this configuration file. We assume it to be
        the same if all sections and key/values match, but the ordering is
        not considered important.

        :return: checksum string
        :rtype: str
        """

        config_parser = ConfigParser.RawConfigParser()
        config_parser.read(self.parsed_value)
        result = {}
        for section in config_parser.sections():
            result[section] = dict(config_parser.items(section))

        return hashsum(result)
