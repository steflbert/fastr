import fastr


def create_network():
    # These featurse are fast to compute, do no use gaussian curvature or hessian eigenvalues!
    # Settings
    # Scales for the guassian scale space filtering
    scales = (1.0, 3.0)
    features = ('orig', 'blur', 'gradient_magnitude', 'laplacian')

    # Number of classes in the segmentation problem
    nrclasses = 4

    # Setup Network and sources
    network = fastr.Network(id_="voxel_classifier_apply")
    target_t1 = network.create_source('NiftiImageFileCompressed', id_='target_t1')
    classifier = network.create_source('SKLearnClassifierFile', id_='classifier')

    # Create scalespacefilter image for T1 using GaussianScaleSpace
    scalespacefilter = network.create_node('GaussianScaleSpace', id_='scalespacefilter', memory='5G')

    scalespacefilter.inputs['image'] = target_t1.output
    scalespacefilter.inputs['scales'] = scales
    scalespacefilter.inputs['features_enum'] = features

    # Apply classifier using ApplyClassifier
    applyclass = network.create_node('ApplyClassifier', id_='applyclass', memory='6G')
    applyclass.inputs['image'] = scalespacefilter.outputs['image']
    applyclass.inputs['classifier'] = classifier.output
    applyclass.inputs['number_of_classes'] = [nrclasses]

    # Tool for picking the correct map based on the max probability using ArgMaxImage
    argmax = network.create_node('ArgMaxImage', id_='argmax')
    argmax.inputs['image'] = applyclass.outputs['probability_image']

    # Create sink
    out_seg = network.create_sink('NiftiImageFileCompressed', id_='out_seg')
    out_seg.input = argmax.outputs['image']

    return network


def main():
    network = create_network()
    network.draw_network(name=network.id, draw_dimension=True)
    sourcedata = {
        'target_t1': [r'vfsregex://tutorial/source/images/test/(?P<id>IBSR_\d\d)_t1.nii.gz'],
        'classifier': ['vfs://tutorial/sink/voxel_classifier_tissue_all.clf']
    }
    sinkdata = {
        'out_seg': 'vfs://tutorial/sink/images/{sample_id}_tissue{ext}'
    }
    network.execute(sourcedata, sinkdata)


if __name__ == '__main__':
    main()
