import fastr


def create_network():
    # Settings
    # Scale space scales to extract (in mm)
    scales = (1.0, 3.0)

    # Radius for the background sampling mask dilation (in voxels?)
    radius = [3]

    # Number/fraction of sample to sample per images
    # On element per class (class0, class1, etc)
    # If value between [0.0 - 1.0] it is a fraction of the number of samples
    # available in that class
    # If the value is above 1, it is the number of samples to take, if the
    # avaialble number of samples is lower, it will take all samples.
    number_of_samples = [5000]

    # Note: the parameters for the random forest classifier are in the
    # parameter file supplied as source data

    # These featurse are fast to compute, do no use gaussian curvature or hessian eigenvalues!
    features = ('orig', 'blur', 'gradient_magnitude', 'laplacian')

    # Setup Network
    network = fastr.Network(id_="voxel_classifier_train")

    # Setup sources
    source_t1 = network.create_source('NiftiImageFileCompressed', id_='source_t1', nodegroup='atlas')
    source_label = network.create_source('NiftiImageFileCompressed', id_='source_label', nodegroup='atlas')
    source_param = network.create_source('ConfigFile', id_='source_param')

    # Create scalespace feature image for T1
    scalespacefilter = network.create_node('GaussianScaleSpace', id_='scalespacefilter', memory='4G')

    # Link the data
    scalespacefilter.inputs['image'] = source_t1.output
    scalespacefilter.inputs['scales'] = scales
    scalespacefilter.inputs['features_enum'] = features

    # Prepare mask for sampling
    threshold = network.create_node('ThresholdImage', id_='threshold', memory='2G')
    morph = network.create_node('Morphology', id_='morph', memory='2G')

    threshold.inputs['image'] = source_label.output
    threshold.inputs['threshold'] = [0.5]

    morph.inputs['image'] = threshold.outputs['image']
    morph.inputs['mode'] = ['dilate']
    morph.inputs['radius'] = radius

    # Sample the feature images
    sampler = network.create_node('SampleImage', id_='sampler', memory='3G')
    sampler.inputs['image'] = scalespacefilter.outputs['image']
    sampler.inputs['labels'] = source_label.output
    sampler.inputs['mask'] = morph.outputs['image']
    sampler.inputs['nsamples'] = number_of_samples

    # Train the classifier, use 8 cores in parallel
    classifier = network.create_node('RandomForestTrain', id_='classifier', memory='2G', cores=2)
    link = network.create_link(sampler.outputs['sample_file'], classifier.inputs['samples'])
    link.collapse = 0
    classifier.inputs['parameters'] = source_param.output
    classifier.inputs['number_of_cores'] = (8,)

    # Create sink
    out_classifier = network.create_sink('SKLearnClassifierFile', id_='out_classifier')
    out_classifier.input = classifier.outputs['classifier']

    return network


def main():
    network = create_network()
    network.draw_network(name=network.id, draw_dimension=True)
    # Set the source and sink data. For the working of the URLs used see
    # http://fastr.readthedocs.org/en/develop/static/reference.html
    sourcedata = {
        'source_t1': [r'vfsregex://tutorial/source/images/train/(?P<id>IBSR_\d\d)_t1.nii.gz'],
        'source_label': [r'vfsregex://tutorial/source/images/train/(?P<id>IBSR_\d\d)_tissue.nii.gz'],
        'source_param': ['vfs://tutorial/source/param/param_single.ini']
    }
    # The parts {sample_id} and {ext} will be substituted by fastr to contain
    # the sample id and extension of the file
    sinkdata = {
        'out_classifier': 'vfs://tutorial/sink/voxel_classifier_tissue_{sample_id}{ext}'
    }

    network.execute(sourcedata, sinkdata)


if __name__ == '__main__':
    main()
