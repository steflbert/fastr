import argparse

import nibabel
import numpy
from scipy.ndimage.morphology import binary_erosion, binary_dilation, binary_closing, binary_opening, generate_binary_structure


def ball_structure(radius):
    n = 2 * radius + 1
    Z, Y, X = numpy.mgrid[-radius:radius:n * 1j,
                          -radius:radius:n * 1j,
                          -radius:radius:n * 1j]
    s = X ** 2 + Y ** 2 + Z ** 2
    return numpy.array(s <= radius * radius, dtype=numpy.uint8)


def morphology(input_path, output_path, method, radius):
    # Prepare data
    input_image = nibabel.load(input_path)
    affine = input_image.get_affine()
    input_data = input_image.get_data().astype(numpy.float32, copy=False)
    if len(input_data.shape) > 3:
        # Drop following 1 sized dimensions
        input_data = input_data.reshape(input_data.shape[:3])

    structure = ball_structure(radius)

    methods = {
        'erode': binary_erosion,
        'dilate': binary_dilation,
        'open': binary_opening,
        'close': binary_closing,
    }

    # Perform the actual dilation
    print('Input data size: {}'.format(input_data.shape))
    print('Structure element size: {}'.format(structure.shape))
    output_data = methods[method](input_data, structure).astype('uint8')

    # Write output data
    output_image = nibabel.Nifti1Image(output_data, affine)
    nibabel.save(output_image, output_path)


def main():
    parser = argparse.ArgumentParser(description='Apply morphology to an image')
    parser.add_argument('-i', '--input', metavar='INPUT.nii.gz',
                        dest='input', type=str, required=True,
                        help='Image to threshold')
    parser.add_argument('-o', '--output', metavar='OUTPUT.nii.gz',
                        dest='output', type=str, required=True,
                        help='Resulting mask')
    parser.add_argument('-m', '--method', metavar='METHOD',
                        dest='method', type=str, required=True,
                        help='The method to use, one of: erode, dilate, open, close')
    parser.add_argument('-r', '--radius', metavar='R',
                        dest='radius', type=int, required=True,
                        help='Radius of structure element')

    args = parser.parse_args()

    # Get args
    input_path = args.input
    output_path = args.output
    method = args.method
    radius = args.radius

    morphology(input_path, output_path, method, radius)


if __name__ == '__main__':
    main()
