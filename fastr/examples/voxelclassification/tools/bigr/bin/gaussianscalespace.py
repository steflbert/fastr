#!/usr/bin/env python
import argparse
import cmath
import gzip
import os
import time

import numpy
from scipy import ndimage, linalg
import nibabel as nib

# For acceleration we use numba, but ignore it if it is not available
NUMBA_LOADED = False


# Mock numba decorator
def jit(*args, **kwargs):
    def wrapper(func):
        return func

    return wrapper


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Create a scale space sampling of an image')
    parser.add_argument('-s', '--scales', metavar='S', dest='scales',
                        type=float, nargs='+', required=True,
                        help='The scales to sample')
    parser.add_argument('-f', '--features', metavar='F', dest='features',
                        type=str, nargs='+', required=False,
                        help='Features to sample, must in {}'.format(GaussianScaleSpace.available_features))
    parser.add_argument('-i', '--in', metavar='IMAGE.NII.GZ', dest='image',
                        type=str, required=True,
                        help='The image to sample')
    parser.add_argument('-l', '--locations', metavar='L.npy.gz', dest='locations',
                        type=str, required=False,
                        help='Locations to sample the features at (will return a 2D array)')
    parser.add_argument('-o', '--outimg', metavar='OUT.nii.gz', dest='outputimg',
                        type=str, required=False,
                        help='The output file to write the 4D image (features in time)')
    parser.add_argument('-a', '--outarray', metavar='OUT.npy.gz', dest='output',
                        type=str, required=False,
                        help='The output file to write (gzipped numpy array)')
    parser.add_argument('-b', '--bspline-order', metavar='N', dest='order',
                        type=int, required=False, default=1,
                        help='The B-spline interpolation order for when locations are used')
    parser.add_argument('-d', '--datatype', metavar='D', dest='datatype',
                        type=str, required=False, help='The datatype for the output data')
    args = parser.parse_args()

    # Settings
    scales = args.scales
    requested_features = args.features
    image = os.path.abspath(os.path.expandvars(os.path.expanduser(args.image)))
    output = args.output
    outputimg = args.outputimg
    order = args.order
    datatype = args.datatype

    if args.locations is not None:
        if args.locations.endswith('.gz'):
            with gzip.open(args.locations) as fin:
                locations = numpy.load(fin)
        else:
            locations = numpy.load(args.locations)

    else:
        locations = None

    if requested_features is None:
        requested_features = GaussianScaleSpace.available_features
    else:
        requested_features = [x for x in requested_features if x in GaussianScaleSpace.available_features]

        if requested_features != args.features:
            print('WARNING: some features requested are not know, dropped some features'
                  'resulting in requested features: {}'.format(requested_features))

    # Load input image
    print('Loading input image')
    img = nib.load(image)

    # Get the feature space
    scale_space = GaussianScaleSpace(img, locations=locations, order=order)
    features = [(x, y) for x in requested_features if x != 'orig' for y in scales]
    if 'orig' in requested_features:
        features = [('orig', None)] + features

    print('Desired features:')
    for nr, feature in enumerate(features):
        print('{}: {}'.format(nr, feature))
    features = scale_space.get_feature_images(features)

    if datatype is not None:
        features = features.astype(datatype)

    if outputimg is not None:
        if locations is not None:
            print('Cannot save image if sampling of specific coordinates is used, this would create useless images!')
        else:
            print('Saving Nifti image...')
            outimage = nib.Nifti1Image(features.reshape(features.shape[:3] + (1,) + features.shape[3:]), img.get_affine())
            nib.save(outimage, outputimg)

    if output is not None:
        print('Saving numpy array...')
        with gzip.open(output, 'w') as output_fh:
            numpy.save(output_fh, features)


def caching(func):
    """
    This decorator caches the value in self._cache to avoid data to be
    computed multiple times. Also this function corrects the sigma for
    the voxel spacing.
    """
    name = func.func_name

    def wrapper(self, sigma):
        if (name, sigma) not in self._cache:
            # Compute the value if not cached
            print('Computing {} at scale {}'.format(name, sigma))
            self._cache[name, sigma] = func(self, sigma)

        return self._cache[name, sigma]

    wrapper.__doc__ = func.__doc__
    return wrapper


class GaussianScaleSpace(object):
    available_features = ['orig', 'blur', 'dx', 'dy', 'dz', 'dxx', 'dxy', 'dxz',
                          'dyy', 'dyz', 'dzz', 'gradient_magnitude',
                          'laplacian', 'gaussian_curvature',
                          'hessian_eigenvalue1', 'hessian_eigenvalue2',
                          'hessian_eigenvalue3']

    def __init__(self, img, locations=None, order=1):
        self.img = img.get_data().astype(numpy.float32)
        # Make sure image is 3D at most
        if len(self.img.shape) > 3:
            self.img = self.img.reshape(self.img.shape[:3])

        self.voxel_spacing = numpy.array(img.get_header().get_zooms()[:3])
        print('[INFO] Voxel spacing: {}'.format(self.voxel_spacing))
        if locations is not None:
            # Map the world coordinates to voxel-based coordinates to sample
            self.coordinates = numpy.inner(numpy.linalg.inv(img.affine[:3, :3]),
                                           locations * [-1, -1, 1] - img.affine[:3, 3])
            print('[INFO] Sampling from locations: {}'.format(self.coordinates))
        else:
            self.coordinates = None

        # Interpolation order [0-5]
        if not 0 <= order <= 5:
            raise ValueError('Interpolation order should be [0-5]')
        self.order = order

        self._cache = {}
        self._eigen_values_symbolic = None

    def __getitem__(self, value):
        feature, sigma = value

        if hasattr(self, feature):
            start = time.time()
            result = getattr(self, feature)(sigma)
            print('Got {} ({}) in {} seconds [size {}]'.format(feature, sigma, time.time() - start, result.shape))
            return result
        else:
            raise KeyError('Requested unknown feature!')

    def get_feature_images(self, features):
        # Combine all features and transpose them to the last dimension
        feature_map = numpy.array([self[feature, scale] for feature, scale in features])
        if self.coordinates is None:
            feature_map = feature_map.transpose((1, 2, 3, 0))
        else:
            feature_map = feature_map.transpose((1, 0))

        return feature_map

    def gaussian_filter(self, sigma, order):
        map = ndimage.filters.gaussian_filter(self.img,
                                              sigma / self.voxel_spacing,
                                              order)

        if self.coordinates is not None:
            return ndimage.interpolation.map_coordinates(map, self.coordinates, mode='nearest', order=self.order)
        else:
            return map

    @caching
    def orig(self, sigma):
        """
        Original values of the image at the selected location
        """
        if sigma is not None:
            raise ValueError('Orignal features cannot be computed at a scale')

        if self.coordinates is not None:
            return ndimage.interpolation.map_coordinates(self.img, self.coordinates, mode='nearest', order=self.order)
        else:
            return self.img

    @caching
    def blur(self, sigma):
        return self.gaussian_filter(sigma, 0)

    @caching
    def dx(self, sigma):
        return self.gaussian_filter(sigma, (1, 0, 0))

    @caching
    def dy(self, sigma):
        return self.gaussian_filter(sigma, (0, 1, 0))

    @caching
    def dz(self, sigma):
        return self.gaussian_filter(sigma, (0, 0, 1))

    @caching
    def dxx(self, sigma):
        return self.gaussian_filter(sigma, (2, 0, 0))

    @caching
    def dxy(self, sigma):
        return self.gaussian_filter(sigma, (1, 1, 0))

    @caching
    def dxz(self, sigma):
        return self.gaussian_filter(sigma, (1, 0, 1))

    @caching
    def dyy(self, sigma):
        return self.gaussian_filter(sigma, (0, 2, 0))

    @caching
    def dyz(self, sigma):
        return self.gaussian_filter(sigma, (0, 1, 1))

    @caching
    def dzz(self, sigma):
        return self.gaussian_filter(sigma, (0, 0, 2))

    @caching
    def gradient_magnitude(self, sigma):
        return numpy.sqrt(self['dx', sigma]**2 + self['dy', sigma]**2 + self['dz', sigma]**2)

    @caching
    def laplacian(self, sigma):
        return self['dxx', sigma] + self['dyy', sigma] + self['dzz', sigma]

    @caching
    def hessian_eigenvalues(self, sigma):
        Mxx = self['dxx', sigma]
        Mxy = self['dxy', sigma]
        Mxz = self['dxz', sigma]
        Myy = self['dyy', sigma]
        Myz = self['dyz', sigma]
        Mzz = self['dzz', sigma]

        eig1 = numpy.zeros_like(Mxx)
        eig2 = numpy.zeros_like(Mxx)
        eig3 = numpy.zeros_like(Mxx)

        start = time.time()
        for index, (xx, xy, xz, yy, yz, zz) in enumerate(zip(Mxx.flat, Mxy.flat, Mxz.flat, Myy.flat, Myz.flat, Mzz.flat)):
            eig1.flat[index], eig2.flat[index], eig3.flat[index] = linalg.eigvalsh(numpy.array([[xx, xy, xz],
                                                                                                [xy, yy, yz],
                                                                                                [xz, yz, zz]]))

        print('Spend {} second on true eig'.format(time.time() - start))
        return numpy.stack((eig1, eig2, eig3), axis=-1)

    @caching
    def hessian_eigenvalue1(self, sigma):
        if not NUMBA_LOADED:
            return self['hessian_eigenvalues', sigma][..., 1]
        else:
            Mxx = self['dxx', sigma]
            Mxy = self['dxy', sigma]
            Mxz = self['dxz', sigma]
            Myy = self['dyy', sigma]
            Myz = self['dyz', sigma]
            Mzz = self['dzz', sigma]

            start = time.time()
            eig1 = numpy.zeros_like(Mxx)
            eigen1(eig1, Mxx, Mxy, Mxz, Myy, Myz, Mzz)
            print('Spend {} second on eig1'.format(time.time() - start))

            return eig1

    @caching
    def hessian_eigenvalue2(self, sigma):
        if not NUMBA_LOADED:
            return self['hessian_eigenvalues', sigma][..., 1]
        else:
            Mxx = self['dxx', sigma]
            Mxy = self['dxy', sigma]
            Mxz = self['dxz', sigma]
            Myy = self['dyy', sigma]
            Myz = self['dyz', sigma]
            Mzz = self['dzz', sigma]

            eig2 = numpy.zeros_like(Mxx)
            return eigen2(eig2, Mxx, Mxy, Mxz, Myy, Myz, Mzz)

    @caching
    def hessian_eigenvalue3(self, sigma):
        if not NUMBA_LOADED:
            return self['hessian_eigenvalues', sigma][..., 1]
        else:
            Mxx = self['dxx', sigma]
            Mxy = self['dxy', sigma]
            Mxz = self['dxz', sigma]
            Myy = self['dyy', sigma]
            Myz = self['dyz', sigma]
            Mzz = self['dzz', sigma]

            eig3 = numpy.zeros_like(Mxx)
            return eigen3(eig3, Mxx, Mxy, Mxz, Myy, Myz, Mzz)

    @caching
    def gaussian_curvature(self, sigma):
        xx = self['dxx', sigma]
        xy = self['dxy', sigma]
        xz = self['dxz', sigma]
        yy = self['dyy', sigma]
        yz = self['dyz', sigma]
        zz = self['dzz', sigma]

        # Calculate eigenvalues for each voxel
        curvature = (xx * yy * zz) - (xx * yz**2) - (xy**2 * zz) + (2 * xy * xz * yz) - (xz**2 * yy)

        return curvature


@jit(nopython=True)
def gradient_magnitude(result, dx, dy, dz):
    for index in range(result.size):
        result.flat[index] = (dx.flat[index]**2 + dy.flat[index]**2 + dz.flat[index]**2)**0.5


@jit(nopython=True)
def eigen1(eig1, dxx, dxy, dxz, dyy, dyz, dzz):
    for index in range(dxx.size):
        xx = dxx.flat[index]
        xy = dxy.flat[index]
        xz = dxz.flat[index]
        yy = dyy.flat[index]
        yz = dyz.flat[index]
        zz = dzz.flat[index]

        eig1.flat[index] = (xx/3.0 + yy/3.0 + zz/3.0 + (xx*yy/3.0 + xx*zz/3.0 - xy**2.0/3.0 - xz**2.0/3.0 + yy*zz/3.0 - yz**2.0/3.0 - (-xx - yy - zz)**2.0/9.0)/(-xx*yy*zz/2 + xx*yz**2/2 + xy**2*zz/2 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2.0/3.0 - xz**2.0/3.0 + yy*zz/3.0 - yz**2.0/3.0 - (-xx - yy - zz)**2.0/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3.0/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2.0/4.0) + (-xx - yy - zz)**3.0/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6.0)**(1.0/3.0) - (-xx*yy*zz/2.0 + xx*yz**2/2.0 + xy**2*zz/2.0 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2/3.0 - xz**2/3.0 + yy*zz/3.0 - yz**2/3.0 - (-xx - yy - zz)**2/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3.0/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2/4.0) + (-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6)**(1.0/3.0)).real
    return eig1


@jit(nopython=True)
def eigen2(eig2, dxx, dxy, dxz, dyy, dyz, dzz):
    for index in range(dxx.size):
        xx = dxx.flat[index]
        xy = dxy.flat[index]
        xz = dxz.flat[index]
        yy = dyy.flat[index]
        yz = dyz.flat[index]
        zz = dzz.flat[index]

        eig2.flat[index] = (xx/3.0 + yy/3.0 + zz/3.0 + (xx*yy/3.0 + xx*zz/3.0 - xy**2.0/3.0 - xz**2.0/3.0 + yy*zz/3.0 - yz**2.0/3.0 - (-xx - yy - zz)**2.0/9.0)/((-1.0/2.0 - cmath.sqrt(3)*1j/2.0)*(-xx*yy*zz/2.0 + xx*yz**2.0/2.0 + xy**2*zz/2.0 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2.0/3.0 - xz**2.0/3.0 + yy*zz/3.0 - yz**2.0/3.0 - (-xx - yy - zz)**2.0/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2/4.0) + (-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6.0)**(1.0/3.0)) - (-1/2.0 - cmath.sqrt(3)*1j/2.0)*(-xx*yy*zz/2.0 + xx*yz**2/2.0 + xy**2*zz/2.0 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2/3.0 - xz**2/3.0 + yy*zz/3.0 - yz**2/3.0 - (-xx - yy - zz)**2/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2/4.0) + (-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6.0)**(1.0/3.0)).real
    return eig2


@jit(nopython=True)
def eigen3(eig3, dxx, dxy, dxz, dyy, dyz, dzz):
    for index in range(dxx.size):
        xx = dxx.flat[index]
        xy = dxy.flat[index]
        xz = dxz.flat[index]
        yy = dyy.flat[index]
        yz = dyz.flat[index]
        zz = dzz.flat[index]

        eig3.flat[index] = (xx/3.0 + yy/3.0 + zz/3.0 + (xx*yy/3.0 + xx*zz/3.0 - xy**2/3.0 - xz**2/3.0 + yy*zz/3 - yz**2/3.0 - (-xx - yy - zz)**2/9.0)/((-1/2.0 + cmath.sqrt(3)*1j/2.0)*(-xx*yy*zz/2.0 + xx*yz**2/2.0 + xy**2*zz/2.0 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2/3.0 - xz**2/3.0 + yy*zz/3.0 - yz**2/3.0 - (-xx - yy - zz)**2/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2/4.0) + (-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6.0)**(1/3.0)) - (-1/2.0 + cmath.sqrt(3)*1j/2.0)*(-xx*yy*zz/2.0 + xx*yz**2/2.0 + xy**2*zz/2.0 - xy*xz*yz + xz**2*yy/2.0 + cmath.sqrt((xx*yy/3.0 + xx*zz/3.0 - xy**2/3.0 - xz**2/3.0 + yy*zz/3.0 - yz**2/3.0 - (-xx - yy - zz)**2/9.0)**3 + (-xx*yy*zz + xx*yz**2 + xy**2*zz - 2*xy*xz*yz + xz**2*yy + 2*(-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/3.0)**2/4.0) + (-xx - yy - zz)**3/27.0 - (-xx - yy - zz)*(xx*yy + xx*zz - xy**2 - xz**2 + yy*zz - yz**2)/6.0)**(1.0/3.0)).real

    return eig3

if __name__ == '__main__':
    main()
