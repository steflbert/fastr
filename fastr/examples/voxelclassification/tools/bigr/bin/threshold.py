import argparse

import nibabel
import numpy


def threshold(input_path, output_path, threshold):
    input_image = nibabel.load(input_path)
    affine = input_image.get_affine()

    input_data = input_image.get_data().astype(numpy.float32, copy=False)

    print('Input data size: {}'.format(input_data.shape))
    output_data = (input_data >= threshold).astype('uint8')
    print('Output data size: {}'.format(output_data.shape))

    output_image = nibabel.Nifti1Image(output_data, affine)
    nibabel.save(output_image, output_path)


def main():
    parser = argparse.ArgumentParser(description='Threshold an image')
    parser.add_argument('-i', '--input', metavar='INPUT.nii.gz',
                        dest='input', type=str, required=True,
                        help='Image to threshold')
    parser.add_argument('-o', '--output', metavar='OUTPUT.nii.gz',
                        dest='output', type=str, required=True,
                        help='Resulting mask')
    parser.add_argument('-t', '--threshold', metavar='N',
                        dest='threshold', type=float, required=True,
                        help='The value on which to threshold')

    args = parser.parse_args()

    # Get args
    input_image = args.input
    output_image = args.output
    threshold_level = args.threshold

    threshold(input_image, output_image, threshold_level)


if __name__ == '__main__':
    main()
