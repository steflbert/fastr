import argparse
import itertools

import numpy
from sklearn import cross_validation
from sklearn import ensemble
from sklearn import grid_search
from sklearn import metrics
from sklearn.externals import joblib

RF_ARGUMENTS = {'n_estimators': int,
                'criterion': str,
                'max_depth': None,
                'min_samples_split': int,
                'min_samples_leaf': int,
                'min_weight_fraction_leaf': float,
                'max_features': (int, float, str),
                'max_leaf_nodes': int,
                'bootstrap': bool,
                'oob_score': bool,
                'random_state': int,
                'verbose': int,
                'warm_start': bool}


def pairwise_auc(reference, estimates, class_i, class_j):
    # Filter out the probabilities for class_i and class_j
    estimates = [est[class_i] for ref, est in zip(reference, estimates) if ref in (class_i, class_j)]
    reference = [ref for ref in reference if ref in (class_i, class_j)]

    # Sort the reference by the estimated probabilities
    sorted_reference = [y for x, y in sorted(zip(estimates, reference), key=lambda p: p[0])]

    # Calculated the sum of ranks for class_i
    sum_rank = 0
    for index, element in enumerate(sorted_reference):
        if element == class_i:
            sum_rank += index + 1
    sum_rank = float(sum_rank)

    # Get the counts for class_i and class_j
    n_class_i = float(reference.count(class_i))
    n_class_j = float(reference.count(class_j))

    # If a class in empty, AUC is 0.0
    if n_class_i == 0 or n_class_j == 0:
        return 0.0

    # Calculate the pairwise AUC
    return (sum_rank - (0.5 * n_class_i * (n_class_i + 1))) / (n_class_i * n_class_j)


def multi_class_auc(reference, estimates):
    classes = numpy.unique(reference)

    if any(t == 0.0 for t in numpy.sum(estimates, axis=1)):
        raise ValueError('No AUC is calculated, output probabilities are missing')

    pairwise_auc_list = [0.5 * (pairwise_auc(reference, estimates, i, j) +
                                pairwise_auc(reference, estimates, j, i)) for i in classes for j in classes]
    c = len(classes) - 1
    return (2.0 * sum(pairwise_auc_list)) / (c * (c - 1))


def main():
    parser = argparse.ArgumentParser(description='Train a random forest classifier')
    parser.add_argument('-s', '--samples', metavar='SAMPLES.npz', dest='samples',
                        type=str, nargs='+', required=True,
                        help='Training samples (multiple files will be concatenated. '
                             'Files must be in numpy .npz format with two arrays'
                             'present: samples and targets.')
    parser.add_argument('-p', '-parameters', metavar='PARAM.txt', dest='param',
                        type=str, required=True,
                        help='RandomForestClassifier parameter file')
    parser.add_argument('-o', '--output', metavar='OUTPUT.clf', dest='output',
                        type=str, required=True,
                        help='Path to save fitted classifier to')
    parser.add_argument('-n', '--nfold', metavar='N', dest='nfold',
                        type=int, required=False, default=10,
                        help='Number of folds in the parameters estimation')
    parser.add_argument('-c', '--cores', metavar='M', dest='cores',
                        type=int, required=False, default=1,
                        help='Number of cores to use for the RFC')
    args = parser.parse_args()

    samples = args.samples
    paramfile = args.param
    output = args.output
    nfold = args.nfold
    ncores = args.cores

    # Load samples
    samples = [_read_samples(x) for x in samples]
    targets = numpy.concatenate([x[1] for x in samples], axis=0)
    samples = numpy.concatenate([x[0] for x in samples], axis=0)

    print('Read samples ({}) and targets ({})'.format(samples.shape, targets.shape))

    if targets.shape[0] != samples.shape[0]:
        raise ValueError('There appears to be a mismatch between samples and targets')

    # Load parameters
    parameters = _read_param(paramfile)

    if len([x for x in itertools.product(*parameters.values())]) > 1:
        print('Taking all combination of parameters for:\n{}'.format(parameters))
        # Fit model using Gridsearch + CV
        cross_validator = cross_validation.StratifiedKFold(targets, nfold)
        rfc = ensemble.RandomForestClassifier(verbose=0)
        multi_class_auc_scorer = metrics.make_scorer(multi_class_auc, needs_proba=True)
        grid_search_cv = grid_search.GridSearchCV(rfc,
                                                  param_grid=parameters,
                                                  scoring=multi_class_auc_scorer,
                                                  cv=cross_validator,
                                                  verbose=1,
                                                  n_jobs=1)
        grid_search_cv.fit(samples, targets)
        model = grid_search_cv.best_estimator_
    else:
        # Fit model using pre-determined parameters
        parameters = {k: v[0] for k, v in parameters.items()}
        print('Using parameters: {}'.format(parameters))

        model = ensemble.RandomForestClassifier(verbose=0, n_jobs=ncores, **parameters)
        model.fit(samples, targets)

    # Save model
    joblib.dump(model, output, compress=4)


def _read_samples(path):
    sample_file = numpy.load(path)

    if 'samples' not in sample_file:
        raise ValueError('The samples .npz file does not contain a samples array')
    if 'targets' not in sample_file:
        raise ValueError('The samples .npz file does not contain a targets array')

    return sample_file['samples'], sample_file['targets']


def _read_param(path):
    parameters = {}

    with open(path, 'r') as param_fh:
        for line in param_fh:
            line = line.strip()

            # Skip empty lines or comments
            if len(line) == 0 or line[0] == '#' or line[0] == '[':
                continue

            # Split line in key and value
            key, value = [x.strip() for x in line.split('=', 1)]

            # Check if key and value are both present
            if len(key) == 0 or len(value) == 0:
                raise ValueError('Malformatted config line: {}'.format(line))

            # Split the value in a list of values
            value = [x.strip() for x in value.split(',') if len(x.strip()) > 0]

            if key not in RF_ARGUMENTS:
                print('Skipping unknown argument "{}" (for random forest)'.format(key))

            desired_type = RF_ARGUMENTS[key]

            if not isinstance(desired_type, tuple):
                desired_type = desired_type,

            parameters[key] = [_cast_to_type(key, x, desired_type) for x in value]

    return parameters


def _cast_to_type(key, value, desired_type):
    for dtype in desired_type:
        try:
            value = dtype(value)
            return value
        except (ValueError, TypeError):
            pass

    raise TypeError('For key {} could not cast value {} to {}'.format(key, value, desired_type))


if __name__ == '__main__':
    main()
