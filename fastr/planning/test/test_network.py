# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple

import pytest

from ..network import Network
from ..node import SourceNode, ConstantNode, Node, SinkNode
from ...abc.serializable import load, save
from ...examples.macro_node import create_network


@pytest.fixture(scope="module")
def network_data():
    populated_network = Network('populated_network')

    source_node1 = populated_network.create_source('Int', id_='source')
    constant_node = populated_network.create_constant('Int', (42, 7, 1337), id_='constant')

    addint = populated_network.create_node('fastr/math/AddInt:1.0',
                                           tool_version='1.0',
                                           id_='testnode_addint')
    div_link = constant_node.output >> addint.inputs['left_hand']
    div_link.expand = True
    addint.inputs['right_hand'] << source_node1.output

    sink_node = populated_network.create_sink('Int', id_='sink')
    sink_node.inputs['input'] = addint.outputs['result']

    empty_network = Network('empty_network')

    macro_network = create_network().parent

    data_container = namedtuple('DataContainer', [
        'populated_network',
        'empty_network',
        'macro_network',
    ])

    return data_container(
        populated_network,
        empty_network,
        macro_network,
    )


def test_network_eq_operator(network_data):
    assert network_data.populated_network == network_data.populated_network
    assert network_data.empty_network == network_data.empty_network
    assert network_data.populated_network != network_data.empty_network


def test_network_getstate_setstate(network_data):
    #  Dump state and rebuild in a clean object
    empty_state = network_data.empty_network.__getstate__()
    empty_rebuilt = Network.__new__(Network)
    empty_rebuilt.__setstate__(empty_state)
    assert network_data.empty_network == empty_rebuilt

    #  Dump state and rebuild in a clean object
    populated_state = network_data.populated_network.__getstate__()
    populated_rebuilt = Network.__new__(Network)
    populated_rebuilt.__setstate__(populated_state)
    assert network_data.populated_network == populated_rebuilt


def test_network_serializing(network_data):
    empty_json = network_data.empty_network.serialize()
    empty_rebuilt = Network.deserialize(empty_json)
    assert network_data.empty_network == empty_rebuilt

    populated_json = network_data.populated_network.serialize()
    populated_rebuilt = Network.deserialize(populated_json)
    assert network_data.populated_network == populated_rebuilt

    assert populated_rebuilt != empty_rebuilt
    assert network_data.empty_network != populated_rebuilt
    assert network_data.populated_network != empty_rebuilt


def test_network_save_load(tmp_path, network_data):
    empty_path = tmp_path / "empty.yaml"
    save(network_data.empty_network, empty_path)
    empty_rebuilt = load(empty_path)
    assert network_data.empty_network == empty_rebuilt

    populated_path = tmp_path / "populated.yaml"
    save(network_data.populated_network, populated_path)
    populated_rebuilt = load(populated_path)
    assert network_data.populated_network == populated_rebuilt
    assert populated_rebuilt != empty_rebuilt

    assert network_data.empty_network != populated_rebuilt
    assert network_data.populated_network != empty_rebuilt


def test_macro_network(network_data):
    macro_network_state = network_data.macro_network.__getstate__()
    macro_network_rebuilt = Network.__new__(Network)
    macro_network_rebuilt.__setstate__(macro_network_state)
    assert network_data.macro_network == macro_network_rebuilt

    macro_network_state2 = network_data.macro_network.__getstate__()
    macro_network_rebuilt2 = Network.__new__(Network)
    macro_network_rebuilt2.__setstate__(macro_network_state2)
    assert network_data.macro_network == macro_network_rebuilt2
    assert macro_network_rebuilt == macro_network_rebuilt2

    assert network_data.macro_network == macro_network_rebuilt
    macro_network_json = network_data.macro_network.serialize()
    macro_network_deserialized = Network.deserialize(macro_network_json)
    assert network_data.macro_network == macro_network_deserialized
