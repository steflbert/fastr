# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple

import pytest

from ..network import Network
from ..node import Node, SourceNode, SinkNode, ConstantNode
from ...abc.serializable import load, save


@pytest.fixture(scope="module")
def node_data():
    data_container = namedtuple('DataContainer', [
        'source_network',
        'addint',
        'sum',
        'source_node',
        'source_node2',
        'sink_node',
        'sink_node2',
        'constant_node',
        'constant_node2',
        'target_network',
    ])

    source_network = Network('source_network')
    return data_container(
        source_network,
        source_network.create_node('fastr/math/AddInt:1.0', id_='testnode_addint', tool_version='1.0'),
        source_network.create_node('fastr/math/Sum:1.0', id_='testnode_sum', tool_version='1.0'),
        source_network.create_source('Int', 'source'),
        source_network.create_source('Int', 'source2'),
        source_network.create_sink('Int', 'sink'),
        source_network.create_sink('Int', 'sink2'),
        source_network.create_constant('Int', [42], id_='constant'),
        source_network.create_constant('Int', [42], id_='constant2'),
        Network('target_network'),
    )


def test_node_eq_operator(node_data):
    assert node_data.addint == node_data.addint
    assert node_data.sum == node_data.sum
    assert node_data.addint != node_data.sum
    assert node_data.sum != node_data.addint


def test_sourcenode_eq_operator(node_data):
    assert node_data.source_node == node_data.source_node
    assert node_data.source_node2 == node_data.source_node2
    assert node_data.source_node != node_data.source_node2
    assert node_data.source_node2 != node_data.source_node


def test_constantnode_eq_operator(node_data):
    assert node_data.constant_node == node_data.constant_node
    assert node_data.constant_node2 == node_data.constant_node2
    assert node_data.constant_node != node_data.constant_node2
    assert node_data.constant_node2 != node_data.constant_node


def test_sinknode_eq_operator(node_data):
    assert node_data.sink_node == node_data.sink_node
    assert node_data.sink_node2 == node_data.sink_node2
    assert node_data.sink_node != node_data.sink_node2
    assert node_data.sink_node2 != node_data.sink_node


def test_node_getstate_setstate(node_data):
    #  Dump state and rebuild in a clean object
    addint_state = node_data.addint.__getstate__()
    addint_state['parent'] = node_data.target_network
    addint_rebuilt = Node.__new__(Node)
    addint_rebuilt.__setstate__(addint_state)
    assert node_data.addint == addint_rebuilt

    #  Dump state and rebuild in a clean object
    sum_state = node_data.sum.__getstate__()
    sum_state['parent'] = node_data.target_network
    sum_rebuilt = Node.__new__(Node)
    sum_rebuilt.__setstate__(sum_state)
    assert node_data.sum == sum_rebuilt


def test_node_serializing(node_data):
    addint_json = node_data.addint.serialize()
    addint_rebuilt = Node.deserialize(addint_json, network=node_data.target_network)
    assert node_data.addint == addint_rebuilt
    assert node_data.sum != addint_rebuilt

    sum_json = node_data.sum.serialize()
    sum_rebuilt = Node.deserialize(sum_json, network=node_data.target_network)
    assert node_data.sum == sum_rebuilt
    assert node_data.addint != sum_rebuilt

    assert addint_rebuilt != sum_rebuilt


def test_source_node_serializing(node_data):
    source_json = node_data.source_node.serialize()
    source_rebuilt = SourceNode.deserialize(source_json, network=node_data.target_network)
    assert node_data.source_node == source_rebuilt


def test_sink_node_serializing(node_data):
    sink_json = node_data.sink_node.serialize()
    sink_rebuilt = SinkNode.deserialize(sink_json, network=node_data.target_network)
    assert node_data.sink_node == sink_rebuilt


def test_constant_node_serializing(node_data):
    constant_json = node_data.constant_node.serialize()
    print(f'CONSTANT JSON: {constant_json}')
    constant_rebuilt = ConstantNode.deserialize(constant_json, network=node_data.target_network)
    assert node_data.constant_node == constant_rebuilt
