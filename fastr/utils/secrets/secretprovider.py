# Copyright 2011-2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import ABCMeta, abstractmethod

class SecretProvider(object, metaclass=ABCMeta):
    @abstractmethod
    def get_credentials_for_machine(self, machine):
        """
        Get the password for a user :param str machine: the machine to find password for :raises CouldNotRetrieveCredentials: if password is not found
        """
        pass

    @abstractmethod
    def set_credentials_for_machine(self, machine, username, password):
        """
        Set the password for a user :param str machine: the machine to find password for :raises CouldNotSetCredentials: if password was not set
        """
        pass

    @abstractmethod
    def del_credentials_for_machine(self, machine):
        """
        Deletes the password for a user :param str machine: the machine to find password for :raises CouldNotDeleteCredentials: if password was not removed
        """
        pass
